package logic;

import java.util.concurrent.ThreadLocalRandom;

public class GameDice {
	
	public static int rollNormalDice() {
		return ThreadLocalRandom.current().nextInt(1, 10 + 1);
	}
}
