package ctrl;

import java.util.ArrayList;
import java.util.HashMap;

import player.AbstractPlayer;

/*
 * Representation :
 * [ Limit ] : [ Player1, Player2, Player3, Player4 ]
 * 
 * The limit represents the value that is required to get to the position
 * Players are placed following their stats in the structure
 * If a player reaches the limit, he is placed in that position
 * 
 * Example :
 * Player = Coins / Stars
 * 
 * P1 = 10 / 0
 * P2 = 0 / 1
 * P3 = 20 / 0 
 * P4 = 30 / 0
 * 
 * Following the logic, the positions would be : P2 > P4 > P3 > P1, so the structure would be :
 * 
 * [ stars = 1, coins = 0 ] : [ P2 ]
 * [ stars = 0, coins = 30 ] : [ P4 ]
 * [ stars = 0, coins = 20 ] : [ P3 ]
 * [ stars = 0, coins = 10 ] : [ P1 ]
 * 
 * if update (moving position), loop again on structure and check if another update is done
 */

public class GameLeaderboard {
	private HashMap<GameLeaderboardLimit, ArrayList<AbstractPlayer>> leaderboard = new HashMap();
	
	public GameLeaderboard() {
		
	}
	
	public void addPlayerToLeaderboard(AbstractPlayer player) {
		// TODO : Add players to map
	}
	
	public void updateLeaderboard() {
		
	}
	
	public HashMap getLeaderboard() {
		return leaderboard;
	}

	private class GameLeaderboardLimit {
		private int coinsLimit = 0;
		private int starsLimit = 0;
		
		public int getCoinsLimit() {
			return coinsLimit;
		}
		
		public void setCoinsLimit(int coinsLimit) {
			this.coinsLimit = coinsLimit;
		}
		
		public int getStarsLimit() {
			return starsLimit;
		}
		
		public void setStarsLimit(int starsLimit) {
			this.starsLimit = starsLimit;
		}
		
		public void movePlayerToPosition(AbstractPlayer player) {
			
		}
	}
}

