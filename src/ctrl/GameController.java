package ctrl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import board.AbstractGameBoard;
import cst.GameConstants;
import factory.GameBoardFactory;
import logic.GameDice;

import java.util.TreeMap;

import player.AbstractPlayer;
import player.HumanPlayer;
import slot.AbstractGameSlot;
import slot.GameSlotEffect;

public class GameController {	
	private GameOutput output;
	private GameState state;
	private GameEffectController effectCtrl;
	private AbstractGameBoard board;
	
	public GameController() {
		output = new GameOutput();
		state = new GameState();
		effectCtrl = new GameEffectController(state);
	}
	
	public void startGame() {
		// setup the board
		setupGameBoard();
		
		// configuration
		setupNumberOfPlayers();
		choosePlayers();
		chooseDifficulty();
		setupNumberOfTurns();
		chooseBonus();
		recapConfiguration();
		
		// setup player order
		setupPlayerOrder();
				
		// main loop
		launchGame();
	}
	
	private void setupGameBoard() {
		try {
			int choice = output.askPlayer("On joue sur quel tableau ?", GameConstants.BOARDS_AVAILABLE);
			String selectedBoardId = GameConstants.BOARDS_AVAILABLE[choice - 1];
			
			board = GameBoardFactory.getInstance().createGameBoard(selectedBoardId);
			board.setController(this);
		} catch (Exception e) {
			e.printStackTrace();
			output.askPlayer("Une erreur est survenue"); 
		}
	}
	
	private void setupNumberOfPlayers() {
		int choice = output.askPlayer("Combien de joueurs ?", GameConstants.NB_PLAYERS_POSSIBLE);
		state.setNbOfPlayers(GameConstants.NB_PLAYERS_POSSIBLE[choice - 1]);
	}
	
	private void choosePlayers() {
		int currentNbOfPlayers = state.getNbOfPlayers();
		
		for(int i = 0; i < currentNbOfPlayers; i++) {			
			int choice = output.askPlayer("Quel est le héros choisi par le joueur " + (i+1) + " ?", GameConstants.HEROES_AVAILABLE);
			HumanPlayer player = new HumanPlayer();
			player.setHero(GameConstants.HEROES_AVAILABLE[choice - 1]);
			
			state.addPlayer(player);
		}
	}
	
	private void chooseDifficulty() {
		
	}
	
	private void setupNumberOfTurns() {
		int choice = output.askPlayer("Combien de tours ?", GameConstants.NB_TURNS_POSSIBLE);
		state.setNbOfTurns(GameConstants.NB_TURNS_POSSIBLE[choice - 1]);
	}
	
	private void chooseBonus() {
		int choice = output.askPlayer("On active les bonus ?", GameConstants.YES_NO_OPTION);
		state.setBonusActive(choice == 1 ? true : false);
	}
	
	private void recapConfiguration() {	
		output.sayToPlayer("Voici la configuration active : ");
		output.sayToPlayer("Nombre de joueurs : " + state.getNbOfPlayers());
		output.sayToPlayer("Nombre de tours : " + state.getNbOfTurns());
		output.sayToPlayer("Bonus " + (state.isBonusActive() ? "activés" : "désactivés"));
		
		int choice = output.askPlayer("Tout ceci est correct ?", GameConstants.YES_NO_OPTION);
		
		if(choice != 1)
			return;
		else
			output.sayToPlayer("C'est parti !");
	}
	
	private void setupPlayerOrder() {
		output.sayToPlayer("Nous allons d'abord déterminer l'ordre des tours");
		
		Map<Integer, HumanPlayer> order = new TreeMap<>(Collections.reverseOrder());
		
		for(int i = 0; i < state.getNbOfPlayers(); i++) {
			HumanPlayer currentPlayer = (HumanPlayer) state.getPlayers().get(i);
			
			output.sayToPlayer("Au tour de " + currentPlayer.getHero() + " de frapper le dé !");
			
			output.askPlayer("Appuyez sur ENTER pour frapper le dé");
			
			int diceValue = -1;
			
			do {
				diceValue = GameDice.rollNormalDice();
			} while(order.containsKey(diceValue));
			
			output.sayToPlayer("Le dé affiche un " + diceValue + " !");
			
			order.put(diceValue, currentPlayer);
		}
		
		output.sayToPlayer("Voici l'ordre des tours :");
		
		for(Entry<Integer, HumanPlayer> entry : order.entrySet()) {
			output.sayToPlayer("Joueur " + entry.getKey() + " : " + entry.getValue().getHero());
			board.setPlayerOrder(entry.getValue());
		}
		
		board.initPlayersStartPosition();
	}
	
	private void launchGame() {
		while(!state.isGameFinished()) {
			output.sayToPlayer("TOUR N° " + state.getCurrentTurn());
			
			for(AbstractPlayer nextPlayer : board.getPlayersInOrder()) {				
				output.sayToPlayer("C'est au tour de " + nextPlayer.getHero() + " de jouer !");
				output.sayToPlayer("[Case n° " + board.getPlayerCurrentSlotId(nextPlayer) + " / Pièces : " + nextPlayer.getCoins() + "]");
				output.askPlayer("Appuyez sur ENTER pour frapper le dé.");
				
				int diceValue = GameDice.rollNormalDice();
				output.sayToPlayer("Le héros " + nextPlayer.getHero() + " avance de " + diceValue 
						+ " case" +	(diceValue == 1 ? "" : "s"));
				
				AbstractGameSlot newPlayerSlot = board.movePlayer(nextPlayer, diceValue);
				
				effectCtrl.applyEffect(nextPlayer, newPlayerSlot);
				
				output.sayToPlayer(nextPlayer.getHero() + " tombe sur la case " + newPlayerSlot.getSlotID() + " et est à " + nextPlayer.getCoins() + " pièces");
				
				output.sayToPlayer("--------------------------------------");
				
				state.updateLeaderboard();
			}
			
			output.sayToPlayer("C'est l'heure du mini-jeu !");
			startMiniGame();
			output.sayToPlayer("==================================");
			
			state.updateLeaderboard();
			state.turnFinished();
		}
		
		launchEndGame();
	}
	
	private void applyCoinsEffect(AbstractPlayer player, String operand, String value) {
		int nbCoins = Integer.parseInt(value);
		
		switch (operand) {
			case "+":
				player.setCoins(player.getCoins() + nbCoins);
				break;
				
			case "-":
				player.setCoins(player.getCoins() - nbCoins);
				break;
	
			default:
				break;
		}
	}
	
	public AbstractGameSlot showIntersection(List<AbstractGameSlot> ways) {
		int choice = output.askPlayer("Quelle route veux-tu prendre ?", ways.toArray());
		
		AbstractGameSlot selected = ways.get(choice-1);
		
		return selected;
	}
	
	private void startMiniGame() {
		
	}
	
	private void launchEndGame() {
		output.sayToPlayer("C'est la fin du jeu ! Voici les résultats :");
		
		int idx = 1;
		
		for(AbstractPlayer p : state.getLeaderboard()) {
			output.sayToPlayer(
					"Position " + idx++ + " : " + p.getHero() + " > [" + p.getStars() + " / " + p.getCoins() + "]");
		}
	}
}