package ctrl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import player.AbstractPlayer;

public class GameState {	
	private int currentTurn = 1;
	private int nbOfTurns = 0;
	private int nbOfPlayers = 0;
	private boolean isBonusActive = false;
	private boolean isMiniGameTime = false;
	
	private List<AbstractPlayer> players = null;
	private GameLeaderboard leaderboard = null;
	
	public GameState() {
		players = new ArrayList<>(4);
		leaderboard = new GameLeaderboard();
	}
	
	public void setNbOfTurns(int nbOfTurns) {
		this.nbOfTurns = nbOfTurns;
	}
	
	public int getNbOfTurns() {
		return nbOfTurns;
	}
	
	public void setNbOfPlayers(int nbOfPlayers) {
		this.nbOfPlayers = nbOfPlayers;
	}
	
	public int getNbOfPlayers() {
		return nbOfPlayers;
	}
	
	public void setBonusActive(boolean isBonusActive) {
		this.isBonusActive = isBonusActive;
	}
	
	public boolean isBonusActive() {
		return isBonusActive;
	}
	
	public void addPlayer(AbstractPlayer newPlayer) {
		players.add(newPlayer);
		leaderboard.addPlayerToLeaderboard(newPlayer);
	}
	
	public List<AbstractPlayer> getPlayers() {
		return players;
	}
	
	public int getCurrentTurn() {
		return currentTurn;
	}
	
	public void turnFinished() {
		currentTurn++;
	}
	
	public boolean isMiniGameTime() {
		return this.isMiniGameTime;
	}
	
	public void setMiniGameTime(boolean isMiniGameTime) {
		this.isMiniGameTime = isMiniGameTime;
	}
	
	public boolean isGameFinished() {
		return currentTurn > nbOfTurns;
	}
	
	public void updateLeaderboard() {
		leaderboard.updateLeaderboard();
	}
	
	public GameLeaderboard getLeaderboard() {
		return leaderboard;
	}	
}
