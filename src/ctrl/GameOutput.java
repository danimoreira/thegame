package ctrl;

import java.util.Scanner;

public class GameOutput {
	
	public void askPlayer(String question) {
		System.out.println(question);
		
		Scanner scan = new Scanner(System.in);
		
		scan.nextLine();
	}
	
	public int askPlayer(String question, Object[] choices) {
		System.out.println(question);
		System.out.println("\n");
		
		for (int i = 0; i < choices.length; i++) {
			System.out.println(i+1 + " : " + choices[i]);
		}
		
		Scanner scan = new Scanner(System.in);
		
		String line = "";
		
		while((line = scan.nextLine()) == "") {
			System.out.println("= Veuillez entrer une commande =");
		}

		return Integer.parseInt(line);
	}
	
	public void sayToPlayer(String statement) {
		System.out.println(statement);
	}
	
}
