package creator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import slot.AbstractGameSlot;
import slot.BonusSlot;
import slot.IntersectionSlot;
import slot.MalusSlot;
import slot.BoardEventSlot;

public class BoardCreator {
	
	public static BoardCreator INSTANCE = null; 
	
	private BoardCreator() { }
	
	public static BoardCreator getInstance() {
		if(INSTANCE == null)
			INSTANCE = new BoardCreator();
		
		return INSTANCE;
	}
	
	public AbstractGameSlot createBoard(File boardMapping) {
		List<SimpleEntry<AbstractGameSlot, String>> slotsToLink = new ArrayList<>();
		
		try(BufferedReader reader = new BufferedReader(new FileReader(boardMapping))){
			String line = "";
			int id = 0;
			
			
			while((line = reader.readLine()) != null) {
				id++;
				
				String[] slotAndLink = line.split(",");
				
				switch (slotAndLink[0]) {
					case "b":
						slotsToLink.add(
								new SimpleEntry<AbstractGameSlot, String>(new BonusSlot(id), slotAndLink[1]));
						break;
					case "r":
						slotsToLink.add(
								new SimpleEntry<AbstractGameSlot, String>(new MalusSlot(id), slotAndLink[1]));			
						break;
					case "v":
						slotsToLink.add(
								new SimpleEntry<AbstractGameSlot, String>(new BoardEventSlot(id), slotAndLink[1]));
						break;
					default:
						slotsToLink.add(
								new SimpleEntry<AbstractGameSlot, String>(new BonusSlot(id), slotAndLink[1]));
						break;
				}
			}			
		} catch (FileNotFoundException e) {
			System.err.println("File not found !");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error while reading the file !");
			e.printStackTrace();
		}
		
		return linkSlots(slotsToLink);
	}
	
	private AbstractGameSlot linkSlots(List<SimpleEntry<AbstractGameSlot, String>> slotsToLink) {
		for(SimpleEntry<AbstractGameSlot, String> entry : slotsToLink) {
			IntersectionSlot slot = (IntersectionSlot) entry.getKey();
			
			String link = entry.getValue();
			
			if(link.contains("-")) {
				List<AbstractGameSlot> intersectionChoices = new ArrayList<>();
				
				String[] splittedLinks = link.split("-");
				
				for(String l : splittedLinks)
					intersectionChoices.add(slotsToLink.get(Integer.parseInt(l) - 1).getKey());
				
				slot.setIntersectionChoices(intersectionChoices);
			} else
				slot.setNext(slotsToLink.get(Integer.parseInt(link) - 1).getKey());
		}
		
		return slotsToLink.get(0).getKey();
	}
	
	private void printBoard() {
		// TODO : Check how to print the board correctly
	}
}
