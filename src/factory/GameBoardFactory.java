package factory;

import board.DarkGameBoard;
import board.DemoGameBoard;
import board.EarthGameBoard;
import board.FireGameBoard;
import board.AbstractGameBoard;
import board.IceGameBoard;
import board.WindGameBoard;
import cst.GameConstants;

public class GameBoardFactory {
	private static GameBoardFactory factory = null;
	
	private GameBoardFactory() {
		
	}
	
	public static GameBoardFactory getInstance() {
		if(GameBoardFactory.factory == null)
			GameBoardFactory.factory = new GameBoardFactory();
		
		return GameBoardFactory.factory;
	}
	
	public AbstractGameBoard createGameBoard(String identifier) throws Exception {
		switch(identifier) {
			case GameConstants.ICE_BOARD_ID:
				return createIceGameBoard();
			case GameConstants.EARTH_BOARD_ID:
				return createEarthGameBoard();
			case GameConstants.FIRE_BOARD_ID:
				return createFireGameBoard();
			case GameConstants.WIND_BOARD_ID:
				return createWindGameBoard();
			case GameConstants.DARK_BOARD_ID:
				return createDarkGameBoard();
			case GameConstants.DEMO_BOARD_ID:
				return createDemoGameBoard();
			default:
				throw new Exception("Board not found in IDs !");
		}
	}
	
	private IceGameBoard createIceGameBoard() {
		return new IceGameBoard();
	}
	
	private EarthGameBoard createEarthGameBoard() {
		return new EarthGameBoard();
	}
	
	private FireGameBoard createFireGameBoard() {
		return new FireGameBoard();
	}
	
	private WindGameBoard createWindGameBoard() {
		return new WindGameBoard();
	}
	
	private DarkGameBoard createDarkGameBoard() {
		return new DarkGameBoard();
	}
	
	private DemoGameBoard createDemoGameBoard() {
		return new DemoGameBoard();
	}
}
