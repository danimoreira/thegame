package player;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPlayer {
	protected int coins = 0;
	protected int stars = 0;
	protected List<Object> items = new ArrayList<>();
	protected String hero = "";
	// protected Dice dice = new NormalDice(); // will be used for when the dice is implemented
	
	public void setHero(String hero) {
		this.hero = hero;
	}
	
	public String getHero() {
		return this.hero;
	}
	
	public void setCoins(int coins) {
		this.coins = coins;
		
		if(this.coins < 0)
			this.coins = 0;
	}
	
	public int getCoins() {
		return this.coins;
	}
	
	public void setStars(int stars) {
		this.stars = stars;
	}
	
	public int getStars() {
		return this.stars;
	}
}
