package main;

import ctrl.GameController;

public class Main {
	public static void main(String[] args) {
		GameController master = new GameController();
		
		master.startGame();
	}
}
