package cst;

public class GameConstants {
	public static final Integer[] NB_TURNS_POSSIBLE = {5, 10, 15};
	public static final Integer[] NB_PLAYERS_POSSIBLE = {1, 2, 3, 4};
	public static final String[] HEROES_AVAILABLE = {"Mario", "Luigi", "Peach", "Donkey"};
	public static final String[] YES_NO_OPTION = {"Oui", "Non"};
	
	// Game slot identifiers
	public static enum SlotType {
			BONUS_SLOT,
			MALUS_SLOT,
			BOARD_EVENT_SLOT,
			BANK_SLOT,
			ITEM_SLOT,
			INTERSECTION_SLOT,
	}
	
	// Game board identifiers
	public static final String ICE_BOARD_ID = "ICE";
	public static final String FIRE_BOARD_ID = "FIRE";
	public static final String EARTH_BOARD_ID = "EARTH";
	public static final String WIND_BOARD_ID = "WIND";
	public static final String DARK_BOARD_ID = "DARK";
	public static final String DEMO_BOARD_ID = "DEMO";
	public static final String[] BOARDS_AVAILABLE = { 
													ICE_BOARD_ID, 
													FIRE_BOARD_ID, 
													EARTH_BOARD_ID,
													WIND_BOARD_ID,
													DARK_BOARD_ID, 
													DEMO_BOARD_ID
													};
}
