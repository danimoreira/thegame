package board;

import java.io.File;

import creator.BoardCreator;

public class DemoGameBoard extends AbstractGameBoard {

	private String demoGameBoardMappingFile = "src/mapping/demoGameBoardMapping.txt";
	
	public DemoGameBoard() {
		this.startingSlot = BoardCreator.getInstance().createBoard(new File(demoGameBoardMappingFile));		
	}
}
