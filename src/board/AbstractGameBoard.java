package board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ctrl.GameController;
import player.AbstractPlayer;
import slot.AbstractGameSlot;
import slot.IntersectionSlot;

public abstract class AbstractGameBoard {
	protected List<AbstractPlayer> orderedPlayerList = new ArrayList<>();
	protected Map<AbstractPlayer, AbstractGameSlot> playerSlotsMap = new HashMap<>();
	
	protected AbstractGameSlot startingSlot;
	protected GameController ctrl;
	
	public void setController(GameController ctrl) {
		this.ctrl = ctrl;
	}
	
	public void setPlayerOrder(AbstractPlayer player) {
		this.orderedPlayerList.add(player);
	}
	
	public List<AbstractPlayer> getPlayersInOrder() {
		return this.orderedPlayerList;
	}
	
	public void initPlayersStartPosition() {
		for(AbstractPlayer player : orderedPlayerList)
			playerSlotsMap.put(player, startingSlot);
	}
	
	public int getPlayerCurrentSlotId(AbstractPlayer player) {
		return this.playerSlotsMap.get(player).getSlotID();
	}
	
	public AbstractGameSlot movePlayer(AbstractPlayer player, int diceValue) {
		return movePlayer(player, this.playerSlotsMap.get(player), diceValue);
	}
	
	public AbstractGameSlot movePlayer(AbstractPlayer player, AbstractGameSlot slot, int diceValue) {
		switch(slot.getSlotType()) {
			case BONUS_SLOT:
				break;
			case MALUS_SLOT:
				break;
			case BANK_SLOT:
				System.out.println("Bank $$$");
				break;
			case BOARD_EVENT_SLOT:
				System.out.println("Board event !");
				break;
			case ITEM_SLOT:
				System.out.println("Item mini-game");
				break;
			case INTERSECTION_SLOT:
				AbstractGameSlot selectedSlot = ctrl.showIntersection(((IntersectionSlot)slot).getIntersectionChoices());
				return movePlayer(player, selectedSlot, diceValue);
			default:
				break;
		}
		
		return (diceValue == 0 ? slot : movePlayer(player, slot.goToNext(), diceValue-1));
	}
}
