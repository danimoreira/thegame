package slot;

import cst.GameConstants.SlotType;

public class BoardEventSlot extends AbstractGameSlot{

	public BoardEventSlot(int id) {
		effect = new GameSlotEffect();
		
		effect.setIdentifier("SPECIAL");
		
		this.slotID = id;
	}
	
	@Override
	public GameSlotEffect getEffect() {
		return effect;
	}

	@Override
	public void setEffect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SlotType getSlotType() {
		return SlotType.BOARD_EVENT_SLOT;
	}
}
