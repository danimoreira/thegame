package slot;

import java.util.ArrayList;
import java.util.List;

import cst.GameConstants.SlotType;

public class IntersectionSlot extends AbstractGameSlot {

	private List<AbstractGameSlot> intersectionChoices = new ArrayList<>();
	
	@Override
	public GameSlotEffect getEffect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEffect() {
		// TODO Auto-generated method stub		
	}

	@Override
	public SlotType getSlotType() {
		return SlotType.INTERSECTION_SLOT;
	}
	
	public List<AbstractGameSlot> getIntersectionChoices() {
		return this.intersectionChoices;
	}
	
	public void setIntersectionChoices(List<AbstractGameSlot> intersectionChoices) {
		this.intersectionChoices = intersectionChoices;
	}

}
