package slot;

import cst.GameConstants.SlotType;

public abstract class AbstractGameSlot {
	protected AbstractGameSlot next;
	protected GameSlotEffect effect;
	protected int slotID = -1;
	
	public int getSlotID() {
		return slotID;
	}
	
	public void setSlotID(int slotID) {
		this.slotID = slotID;
	}
	
	public void setNext(AbstractGameSlot slot) {
		this.next = slot;
	}
	
	public abstract GameSlotEffect getEffect();
	
	public abstract void setEffect();
	
	public abstract SlotType getSlotType();
	
	public AbstractGameSlot goToNext() {
		return next;
	}	
	
	public String toString() {
		return "(" + slotID + ")[" + this.effect.getIdentifier() + this.effect.getOperand() + this.getEffect().getValue() + "]";
	}
}
