package slot;

import cst.GameConstants.SlotType;

public class MalusSlot extends AbstractGameSlot {

	public MalusSlot(int id) {
		effect = new GameSlotEffect();
		
		effect.setIdentifier("COINS");
		effect.setOperand("-");
		effect.setValue("3");
		
		this.slotID = id;
	}
	
	@Override
	public GameSlotEffect getEffect() {
		return effect;	
	}

	@Override
	public void setEffect() {
		// TODO Auto-generated method stub		
	}

	@Override
	public SlotType getSlotType() {
		return SlotType.MALUS_SLOT;
	}
}