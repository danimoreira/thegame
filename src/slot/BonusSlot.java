package slot;

import cst.GameConstants.SlotType;

public class BonusSlot extends AbstractGameSlot {	
	public BonusSlot(int id) {
		effect = new GameSlotEffect();
		
		effect.setIdentifier("COINS");
		effect.setOperand("+");
		effect.setValue("3");
		
		this.slotID = id;
	}
	
	@Override
	public GameSlotEffect getEffect() {
		return effect;
	}

	@Override
	public void setEffect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SlotType getSlotType() {
		return SlotType.BONUS_SLOT;
	}
}
